# Entorno local

SO: Ubuntu 20.04.1 LTS
Version del SDK de .NET: 5.0.100

# Correr la aplicación

- Levantar MongoDB
$ sudo docker-compose up -d

- Compilar la aplicación
$ dotnet build

- Levantar la API
$ ./run.sh

